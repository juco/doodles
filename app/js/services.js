angular.module('doodles.services', [])

  .service('Doodle', function() {
    function Doodle(props) {
      this.name = props.name || 'unnamed';
      this.team = props.team || 'foo';
      this.attributes = props.attributes;
    }

    Doodle.prototype.topRating = function() {
      return Math.max(
        this.attributes.speed,
        Math.max(this.attributes.strength, this.attributes.intelligence)
      );
    };

    Doodle.prototype.formatName = function() {
      return this.name + ':bug';
    };

    return Doodle;
  })

  .service('selectionManager', function() {
    var selected = []
      , maxItems = 2; // This could be neatly configurable

    this.add = function(doodle) {
      if (selected.length >= maxItems) {
        throw new Error('You can only select ' + maxItems + ' items at once');
      }

      selected.push(doodle);
    };

    this.remove = function(doodle) {
      selected = selected.filter(function(item) {
        return item !== doodle;
      });
    };

    this.isSelected = function(doodle) {
      return selected.some(function(item) {
        return item === doodle;
      });
    };

    this.selected = function() {
      return selected;
    };
  })

  .factory('doodles', function($http, $q, Doodle) {
    var doodles;

    this.fetch = function() {
      return $http.get('/api/doodles.json')
        .then(function(res) {
          doodles = res.data.map(function(item) {
              return new Doodle(item);
            });

          return doodles;
        });
    };

    this.sort = function(sortBy, sortDir) {
      sortBy = sortBy || 'name';
      sortDir = sortDir || 'asc';
      return doodles.sort(sortCb(sortBy, sortDir));
    };

    function sortCb(key, dir) {
      return function(a, b) {
        if (a[key] > b[key] && dir === 'asc') { return 1; }
        if (a[key] < b[key] && dir === 'asc') { return -1; }
        if (a[key] > b[key] && dir === 'desc') { return -1; }
        if (a[key] < b[key] && dir === 'desc') { return 1; }
        return 0; // ¯\(°_o)/¯
      };
    }

    return {
      fetch: this.fetch,
      sort: this.sort
    };
  });
