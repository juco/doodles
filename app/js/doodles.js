angular.module('doodles', [
  'ngRoute',

  'doodles.services',
  'doodles.controllers',
  'doodles.directives'
])

  .config(function($routeProvider) {
    $routeProvider
      .otherwise({
        controller: 'HomeCtrl',
        templateUrl: 'partial/home/index.html'
        //resolve: function() {
        //  return {
        //    doodles: Doodle.fetch();
        //  }
        //}
      });
  });
