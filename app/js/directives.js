angular.module('doodles.directives', [])

  .directive('doodleTable', function(doodles, selectionManager) {
    return {
      restrict: 'EA',
      templateUrl: '/partial/doodle/table.html',
      scope: {
        doodles: '='
      },
      link: function(scope) {
        scope.orderedBy = 'name';
        scope.orderedDir = 'asc';

        scope.checked = function(doodle, ev) {
          try {
            if (selectionManager.isSelected(doodle)) {
              selectionManager.remove(doodle);
            } else {
              selectionManager.add(doodle);
            }
          } catch(error) {
            // Should probably notify the user of this
            ev.preventDefault();
          }
        };

        scope.sortBy = function(key) {
          // Default to Ascending when selecting a new column
          if (scope.orderedBy === key) {
            if (scope.orderedDir === 'asc') {
              scope.orderedDir = 'desc';
            } else {
              scope.orderedDir = 'asc';
            }
          }

          scope.orderedBy = key;
          scope.doodles = doodles.sort(scope.orderedBy, scope.orderedDir);
        };
      }
    };
  })

  .directive('doodleComparison', function(selectionManager) {
    return {
      restrict: 'EA',
      templateUrl: '/partial/doodle/comparison.html',
      link: function(scope) {
        scope.$watch(function() {
          return selectionManager.selected();
        }, function(newVal, oldVal) {
          if (newVal !== oldVal) {
            scope.selected = newVal;
          }
        }, true);
      }
    };
  });
