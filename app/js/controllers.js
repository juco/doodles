angular.module('doodles.controllers', [])

  .controller('HomeCtrl', function($scope, doodles) {
    // In reality we would of course need a .catch() that informs
    // the user of the failure
    doodles.fetch()
      .then(function(res) {
        $scope.doodles = res;
      });
  });
