describe('Doodle service', function() {
  beforeEach(module('doodles.services'));

  describe('Doodle', function() {
    var Doodle
      , doodleInstance;

    beforeEach(inject(function($injector) {
      Doodle = $injector.get('Doodle');
      doodleInstance = new Doodle({
        name: 'mono',
        team: 'A-team',
        attributes: {
          speed: 10,
          strength: 20,
          intelligence: 30
        }
      });
    }));

    it('should return the correct properties', function() {
      expect(doodleInstance.name).toEqual('mono');
      expect(doodleInstance.team).toEqual('A-team');
      expect(doodleInstance.attributes.speed).toEqual(10);
      expect(doodleInstance.attributes.strength).toEqual(20);
      expect(doodleInstance.attributes.intelligence).toEqual(30);
    });

    it('should provide a default name if not provided', function() {
      expect(new Doodle({}).name).toEqual('unnamed');
    });

    it('should provide a default team if not provided', function() {
      expect(new Doodle({}).team).toEqual('foo');
    });

    it('should format the name', function() {
      expect(doodleInstance.formatName()).toEqual('mono:bug');
    });

    it('should return the maximum rating', function() {
      expect(doodleInstance.topRating()).toEqual(30);
    });
  });

  describe('selectionManager', function() {
    var selectionManager;

    beforeEach(inject(function($injector) {
      selectionManager = $injector.get('selectionManager');
    }));

    describe('add() method', function() {
      it('should allow for adding items', function() {
        selectionManager.add({ name: 'foo' });
        expect(selectionManager.selected().length).toEqual(1);
        selectionManager.add({ name: 'bar' });
        expect(selectionManager.selected().length).toEqual(2);
      });

      it('should not allow for the adding of more than 2 items', function() {
        selectionManager.add({ name: 'foo' });
        selectionManager.add({ name: 'bar' });
        expect(function() {
          selectionManager.add({ name: 'moo' });
        }).toThrowError(/only select 2/);
      });
    });

    describe('remove() method', function() {
      it('should remove items', function() {
        var a = { foo: 'foo' };
        var b = { foo: 'bar' };
        selectionManager.add(a);
        selectionManager.add(b);

        selectionManager.remove(a);
        expect(selectionManager.selected().length).toEqual(1);
        selectionManager.remove(b);
        expect(selectionManager.selected().length).toEqual(0);
      });

      it('should remove the correct items', function() {
        var a = { foo: 'foo' };
        var b = { foo: 'bar' };
        selectionManager.add(a);
        selectionManager.add(b);

        selectionManager.remove(a);
        expect(selectionManager.selected()[0].foo).toEqual('bar');
        selectionManager.remove({ a: 'b' });
        expect(selectionManager.selected().length).toEqual(1);
      });
    });

    describe('isSelected() method', function() {
      it('should correctly return true for selected values', function() {
        var a = { foo: 'foo' };
        var b = { foo: 'bar' };
        selectionManager.add(a);
        expect(selectionManager.isSelected(a)).toEqual(true);
        expect(selectionManager.isSelected(b)).toEqual(false);
      });
    });
  });

  describe('doodles', function() {
    var doodles
      , $httpBackend
      , mockResponse;

    mockResponse = [{
      name: 'mono',
      team: 'foo',
      attributes: { speed: 46 }
    }, {
      name: 'evil',
      team: 'doo',
      attributes: { speed: 53 }
    }];

    beforeEach(inject(function($injector) {
      $httpBackend = $injector.get('$httpBackend');
      doodles = $injector.get('doodles');
    }));

    it('should fetch results from the API', function() {
      var response;

      $httpBackend.expectGET('/api/doodles.json').respond(mockResponse);
      doodles.fetch().then(function(res) {
        response = res;
      });
      $httpBackend.flush();

      expect(response.length).toEqual(2);
    });
  });
});
