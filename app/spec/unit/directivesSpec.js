describe('Doodle directives', function() {
  var $scope
    , $compile
    , $httpBackend
    , mockDoodlesArray
    , mockDoodles
    , mockSelectionManager;

  mockDoodlesArray = [{
    name: 'foo',
    team: 'bar',
    atttribute: {
      speed: 10,
      strength: 20,
      intelligence: 30
    }
  }];

  mockDoodles = {
    sort: angular.noop
  };

  beforeEach(module('doodles', function($provide) {
    $provide.value('doodles', mockDoodles);
    $provide.value('selectionManager', mockSelectionManager);
  }));

  beforeEach(inject(function($injector) {
    $scope = $injector.get('$rootScope').$new();
    $compile = $injector.get('$compile');
    $httpBackend = $injector.get('$httpBackend');
  }));

  beforeEach(function() {
    $httpBackend.whenGET('/partial/doodle/table.html')
      .respond(200, '');
  });

  describe('doodleTable', function() {
    var el
      , isolateScope;


    beforeEach(function() {
      $scope.mockDoodlesArray = mockDoodlesArray;
      el = $compile('<doodle-table doodles="mockDoodlesArray"></doodle-table>')($scope);
      $httpBackend.flush();
      $scope.$digest();
      isolateScope = el.isolateScope();
    });

    it('should defined required methods on $scope', function() {
      expect(isolateScope.checked).toBeDefined();
      expect(isolateScope.sortBy).toBeDefined();
    });

    it('should fetch a sorted list of doodles', function() {
      spyOn(mockDoodles, 'sort');
      isolateScope.sortBy('team');
      expect(isolateScope.orderedBy).toEqual('team');
      expect(isolateScope.orderedDir).toEqual('asc');
      expect(mockDoodles.sort).toHaveBeenCalledWith('team', 'asc');
    });

    it('should have more tests than this....', function() {

    });
  });
});
