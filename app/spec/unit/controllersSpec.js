var $scope
  , $controller
  , $q;

var mockDoodle = function(name) {
  this.name = name;
};

var mockDoodles = {
  fetch: angular.noop
};

describe('Doodle controllers', function() {
  describe('HomeCtrl', function() {

    beforeEach(module('doodles.controllers'));

    beforeEach(inject(function($injector) {
      $scope = $injector.get('$rootScope').$new();
      $controller = $injector.get('$controller');
      $q = $injector.get('$q');
    }));

    beforeEach(function() {
      spyOn(mockDoodles, 'fetch').and.returnValue($q.when([
        new mockDoodle('foo'),
        new mockDoodle('bar')
      ]));

      $controller('HomeCtrl', {
        $scope: $scope,
        doodles: mockDoodles
      });
    });

    it('should fetch the doodles on load', function() {
      expect(mockDoodles.fetch).toHaveBeenCalled();
    });

    it('should assign doodles to the $scope', function() {
      $scope.$apply();

      expect($scope.doodles instanceof Array).toEqual(true);
      expect($scope.doodles[1].name).toEqual('bar');
    });
  });
});
